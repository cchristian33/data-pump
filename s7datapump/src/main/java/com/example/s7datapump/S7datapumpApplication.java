package com.example.s7datapump;

import com.example.s7datapump.cache.CacheService;
import com.example.s7datapump.s7Collector.S7PlcListenerService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

@SpringBootApplication
@EnableScheduling
public class S7datapumpApplication {

    private static CacheService cacheService;

    @Bean
    public TaskScheduler taskScheduler() {
        final ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(10);
        return scheduler;
    }

    @Bean
    public static ObjectMapper jsonObjectMapper() {
        ObjectMapper mapper =  new ObjectMapper();
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        return mapper;
    };

    @Bean
    public static CacheService cacheService() {
         if (cacheService == null) {
             cacheService = new CacheService();
         }
        return cacheService;
    }

    @Value("${datapump.write.to.db.enabled}")
    private boolean enabledWrite;

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job writeDataJob;

    private AtomicInteger batchRunCounter = new AtomicInteger(0);

    @Autowired
    private S7PlcListenerService s7PlcListenerService;

    public static void main(String[] args) {
        SpringApplication.run(S7datapumpApplication.class, args);
    }

    @Scheduled(fixedRate = 1000)
    public void collect() {
        s7PlcListenerService.listen();
    }

    @Scheduled(fixedRate = 60000)
    public void launchJob() throws Exception {
        if (enabledWrite) {
            Date date = new Date();
            jobLauncher.run(writeDataJob, new JobParametersBuilder()
                    .addDate("launchDate", date)
                    .toJobParameters());
            batchRunCounter.incrementAndGet();
        }
    }
}
