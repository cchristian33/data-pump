package com.example.s7datapump.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
//import org.hibernate.search.annotations.Field;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ScannerRecord implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;

//    @Field
    private String barcode;

//    @Field
    private String scanner;

    private String messageId;
    private Timestamp scanTime;
    private Timestamp insertTime;

//    @Field
    private boolean writtenToDb = false;

    @Override
    public String toString() {
        return String.format("Barcode : %s, Scanner : %s, MessageID: %s, ScanTime: %s", barcode, scanner, messageId, scanTime);
    }

    @Override
    public int hashCode() {
        return 1;
    }

    @Override
    public boolean equals(Object o) {
        return true;
    }
}
