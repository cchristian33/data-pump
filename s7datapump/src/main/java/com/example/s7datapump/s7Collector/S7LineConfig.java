package com.example.s7datapump.s7Collector;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class S7LineConfig implements Serializable {
    private int line;
    private int scanners;
    private int startByte;
    private boolean enabled;
    private List<Integer> excludedScanners;

    public int getMaxOffset() {
        return startByte + (S7ConnectionFactory.SCANNER_OFFSET * scanners) + (S7ConnectionFactory.LINE_OFFSET + 8);
    }
}