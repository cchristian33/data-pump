package com.example.s7datapump.s7Collector;

import com.example.s7datapump.S7datapumpApplication;
import com.example.s7datapump.cache.CacheService;
import com.example.s7datapump.entity.ScannerRecord;
import com.github.s7connector.api.DaveArea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Component
public class S7PlcListenerService {
    Logger logger = LoggerFactory.getLogger(S7PlcListenerService.class);

    @Autowired
    private S7ConnectionFactory s7ConnectionFactory;

    public void listen() {
        try {
            s7ConnectionFactory.getConnectors().parallelStream().forEach(connector -> {
                long messageId = System.currentTimeMillis();
                for (S7LineConfig lineConfig : connector.getConfig().getLines()) {
                    int maxOffset = lineConfig.getMaxOffset();
                    long start = System.currentTimeMillis();
                    byte[] bs = connector.getS7Connector().read(DaveArea.DB, 90, maxOffset - lineConfig.getStartByte(), lineConfig.getStartByte());
                    long now = System.currentTimeMillis();
                    logger.info("Read Ended in {} milliseconds for {} -------------------------------", (now - start), connector.getConfig().getName());

                    if (bs != null && bs.length >= 1) {
                        extractAndCache(lineConfig, bs, now, messageId);
                    }
                }
            });
        } catch (ArrayIndexOutOfBoundsException e) {
            logger.warn("An");
        }
//        connector.close();
        //Close connection
    }

    private void extractAndCache(S7LineConfig config, byte[] bs, long now, long messageId) {
        Map<String, ScannerRecord> entries = new HashMap<>();
        for (int i =1; i <= config.getScanners(); i++) {
            String barcode = extractBarcode(i, bs);
            String scannerName = extractScannerName(i, bs);
            scannerName = String.format("%s_"+scannerName, config.getLine());
            String lastCachedScannerValue = S7datapumpApplication.cacheService().getLastCachedScannerValue(scannerName);
            if (lastCachedScannerValue != null && lastCachedScannerValue.equals(barcode)) {
                logger.info("Value for scanner {} has not changed, moving on......", scannerName);
            } else {
//                logger.info("Caching {} for scanner {}", barcode, scannerName);
                ScannerRecord scannerRecord = buildScannerRecord(barcode, scannerName, now);
                scannerRecord.setMessageId(String.valueOf(messageId));
                entries.put(scannerRecord.getId(), scannerRecord);
            }
        }
        if (!entries.isEmpty()) {
            S7datapumpApplication.cacheService().cacheScannerRecords(entries);
        }
    }

    private ScannerRecord buildScannerRecord(String barcode, String scanner, long scanTime) {
        ScannerRecord scannerRecord = new ScannerRecord();
        scannerRecord.setScanner(scanner);
        scannerRecord.setBarcode(barcode);
        scannerRecord.setId(String.format("R_%s_%s", System.currentTimeMillis(), barcode));
        scannerRecord.setScanTime(new Timestamp(scanTime));
        return scannerRecord;
    }

    private String extractBarcode(int scanner, byte[] bytes) {
        int i = S7ConnectionFactory.SCANNER_OFFSET * (scanner - 1) + 50;
        byte[] bytes1 = Arrays.copyOfRange(bytes, i, i + 8);
        return new String(bytes1, StandardCharsets.UTF_8);
    }

    private String extractScannerName(int scanner, byte[] data) {
        int i = S7ConnectionFactory.SCANNER_OFFSET * (scanner - 1) + 6;
        byte[] bytes = Arrays.copyOfRange(data, i, i + 8);
        String s = new String(bytes, StandardCharsets.UTF_8);
        return s;
    }
}

