package com.example.s7datapump.s7Collector;

import com.github.s7connector.api.S7Connector;
import lombok.Getter;

@Getter
public class S7PlcConnector {
    private S7Connector s7Connector;
    private S7Config config;

    public S7PlcConnector(S7Connector connector, S7Config item) {
        this.config = item;
        this.s7Connector = connector;
    }
}
