package com.example.s7datapump.s7Collector;

import com.example.s7datapump.S7datapumpApplication;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.type.TypeReference;
import com.github.s7connector.api.S7Connector;
import com.github.s7connector.api.factory.S7ConnectorFactory;
import com.github.s7connector.exception.S7Exception;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;



@Component
public class S7ConnectionFactory {
    Logger logger = LoggerFactory.getLogger(S7ConnectionFactory.class);

    @Value("${datapump.plc.config}")
    private String configFile;

    public static final int LINE_OFFSET = 50;
    public static final int SCANNER_OFFSET= 72;
    private static List<S7PlcConnector> connectors;

    public List<S7PlcConnector> getConnectors() {
        if (connectors == null || connectors.isEmpty()) {
            connectors = new ArrayList<>();
            File config = new File(configFile);
            FileInputStream inputStream = null;
            try {
                inputStream = new FileInputStream(config);
                InputStreamReader reader = new InputStreamReader(inputStream);
                List<S7Config> data = S7datapumpApplication.jsonObjectMapper().readValue(reader, new TypeReference<List<S7Config>>() {});

                if (data != null && !data.isEmpty()) {
                    data.forEach(item -> {
                        S7Connector connector =
                                S7ConnectorFactory
                                        .buildTCPConnector()
                                        .withHost(item.getIp())
                                        .withRack(0) //optional
                                        .withSlot(3) //optional
                                        .build();

                        connectors.add(new S7PlcConnector(connector, item));
                    });
                }
            } catch (IOException | S7Exception e) {
                logger.warn(e.getMessage());
            }
        }
        return connectors;
    }

}

