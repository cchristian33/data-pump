package com.example.s7datapump.s7Collector;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class S7Config implements Serializable {
    private String name;
    private String ip;
    private int scanners;
    private boolean enabled;
    private List<S7LineConfig> lines;

//    public int getMaxOffset() {
//        S7LineConfig s7LineConfig = lines.get(lines.size() - 1); //ensure this is the max line in the collection
//        return s7LineConfig.getStartByte() + (S7ConnectionFactory.SCANNER_OFFSET * s7LineConfig.getScanners()) + (S7ConnectionFactory.LINE_OFFSET + 8);
//    }
}
