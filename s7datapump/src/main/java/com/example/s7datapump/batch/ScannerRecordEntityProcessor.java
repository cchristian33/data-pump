package com.example.s7datapump.batch;

import com.example.s7datapump.entity.ScannerRecord;
import org.springframework.batch.item.ItemProcessor;

import java.sql.Timestamp;

public class ScannerRecordEntityProcessor implements ItemProcessor<ScannerRecord, ScannerRecord> {

    @Override
    public ScannerRecord process(ScannerRecord item) throws Exception {
        item.setInsertTime(new Timestamp(System.currentTimeMillis()));
        return item;
    }
}
