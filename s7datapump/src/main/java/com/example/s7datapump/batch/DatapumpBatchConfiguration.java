package com.example.s7datapump.batch;

import com.example.s7datapump.cache.CacheService;
import com.example.s7datapump.entity.ScannerRecord;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class DatapumpBatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    private JobCompletionNotificationListener listener;

    @Autowired
    private InfinispanCacheCollector infinispanCacheCollector;

//    @JobScope
    @Bean
    public InfinispanCacheCollector itemReader() {
        infinispanCacheCollector.setName(CacheService.NAME);
        return infinispanCacheCollector;
    }

    @Bean
    public JdbcBatchItemWriter<ScannerRecord> writer(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<ScannerRecord>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("INSERT INTO scanner_record (id, barcode, scanner, message_id, scan_time, insert_time, written_to_db) VALUES (:id, :barcode, :scanner, :messageId, :scanTime, :insertTime, :writtenToDb)")
                .dataSource(dataSource)
                .build();
    }

    @Bean
    public ScannerRecordEntityProcessor processor() {
        return new ScannerRecordEntityProcessor();
    }

    @Bean
    public Job writeDataJob(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1(JdbcBatchItemWriter<ScannerRecord> writer) {
        return stepBuilderFactory.get("step1")
                .<ScannerRecord, ScannerRecord> chunk(1000)
                .reader(itemReader())
                .processor(processor())
                .writer(writer)
                .build();
    }
}
