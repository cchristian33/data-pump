package com.example.s7datapump.batch;

import com.example.s7datapump.S7datapumpApplication;
import com.example.s7datapump.entity.ScannerRecord;
import org.springframework.batch.item.support.AbstractItemCountingItemStreamItemReader;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class InfinispanCacheCollector extends AbstractItemCountingItemStreamItemReader<ScannerRecord> {

    private List<ScannerRecord> data;

    @Override
    protected ScannerRecord doRead() {
        if (data== null ||data.isEmpty()) {
            return null;
        } else if (data.size() == 1) {
            return data.get(getCurrentItemCount());
        } else {
            return data.get(getCurrentItemCount()-1);
        }
    }

    @Override
    protected void doOpen() {
        data = S7datapumpApplication.cacheService().getAllUnpersistedRecords();
        if (data != null && data.size() > 1) {
            setMaxItemCount(data.size());
        } else {
            setMaxItemCount(1);
        }
    }

    @Override
    protected void doClose() {
        //data had been written to the db, so if data is not empty update the cache with this info
        updateCache();
//        setMaxItemCount(0);
        setCurrentItemCount(0);
        if (data != null) {
            data.clear();
        }
    }

    private void updateCache() {
        Map<String, ScannerRecord> dataModified = new HashMap<>();
        if (data != null && !data.isEmpty()) {
            data.forEach(item -> {
                item.setWrittenToDb(true);
                dataModified.put(item.getId(), item);
            });

            S7datapumpApplication.cacheService().cacheScannerRecords(dataModified);
        } else {
            //log the fact that there are no entries in data to update and move on
        }
    }
}
