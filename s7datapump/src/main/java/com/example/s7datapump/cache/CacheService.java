package com.example.s7datapump.cache;

import com.example.s7datapump.entity.ScannerRecord;
import org.infinispan.CacheSet;
import org.infinispan.manager.EmbeddedCacheManager;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CacheService {

    public static final String NAME = "DataPumpOperations";
    public static final String RECORD_CACHE = "DataPumpOperations_DATA";
    public static final String SCANNER_CACHE = "DataPumpOperations_SCANNERS";

    @Autowired
    private EmbeddedCacheManager cacheManager;

    public void cacheScannerRecords(Map<String, ScannerRecord> entries) {
        cacheManager.getCache(RECORD_CACHE).putAll(entries);
    }

    public List<ScannerRecord> getAllUnpersistedRecords() {
//        QueryFactory queryFactory = Search.getQueryFactory(cacheManager.getCache(RECORD_CACHE));
//        Query<ScannerRecord> writtenToDb = queryFactory.from(ScannerRecord.class)
//                .having("writtenToDb").eq(false)
//                .toBuilder().build();
//        List<ScannerRecord> list = writtenToDb.list();
//        return list;
        List<ScannerRecord> records = new ArrayList<>();
        CacheSet<Map.Entry<Object, Object>> entries = cacheManager.getCache(RECORD_CACHE).entrySet();
        entries.forEach(objectObjectEntry -> {
            ScannerRecord record = null;
            try {
                record = (ScannerRecord) objectObjectEntry.getValue();
            } catch (ClassCastException e) {
                //log this here and recover
            }

            if (record != null && !record.isWrittenToDb() && record.getId() != null && record.getId().length() > 2 && !record.getBarcode().equalsIgnoreCase("        ")) {
                records.add(record);
            }
        });
        return records;
    }

    public String getLastCachedScannerValue(String scannerName) {
        return (String) cacheManager.getCache(SCANNER_CACHE).get(scannerName);
    }
}
