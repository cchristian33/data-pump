package com.example.s7datapump.cache;

import com.example.s7datapump.entity.ScannerRecord;
import org.infinispan.commons.marshall.JavaSerializationMarshaller;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.spring.starter.embedded.InfinispanCacheConfigurer;
import org.infinispan.spring.starter.embedded.InfinispanGlobalConfigurationCustomizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Timestamp;

@Configuration
public class InfinispanCacheConfiguration {

    @Autowired
    private static EmbeddedCacheManager defaultCacheManager;

    @Bean
    public InfinispanGlobalConfigurationCustomizer globalCustomizer() {
        return builder -> {
            builder.serialization()
                    .marshaller(new JavaSerializationMarshaller())
                    .whiteList()
                    .addClass(ScannerRecord.class.getName())
                    .addClass(Timestamp.class.getName());
        };
    }

    @Bean
    public InfinispanCacheConfigurer cacheConfigurer() {
        return cfg -> {
            org.infinispan.configuration.cache.Configuration ispnConfig = new ConfigurationBuilder()
                    .encoding().key().mediaType("application/x-java-object")
                    .encoding().value().mediaType("application/x-java-object")
//                    .encoding().value().mediaType("application/x-java-serialized-object")
//                    .clustering()
//                    .cacheMode(CacheMode.LOCAL)
                    .persistence()
                    .passivation(false)
                    .addSingleFileStore()
                    .preload(false)
                    .shared(false)
                    .fetchPersistentState(true)
                    .ignoreModifications(false)
                    .purgeOnStartup(false)
                    .location("src/main/resources/cache")
//                    .async()
//                    .enabled(true)
//                    .threadPoolSize(5)
                    .build();

            cfg.defineConfiguration(CacheService.RECORD_CACHE, ispnConfig);
            cfg.defineConfiguration(CacheService.SCANNER_CACHE, new ConfigurationBuilder().build());
        };
    }
}
