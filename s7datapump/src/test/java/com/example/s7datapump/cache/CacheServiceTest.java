package com.example.s7datapump.cache;

import com.example.s7datapump.entity.ScannerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CacheServiceTest {

    @Autowired
    private CacheService cacheService;

    @Autowired
    private CacheService cacheService2;

    @Test
    void cacheScannerRecords() {
        List<ScannerRecord> allRecords = cacheService2.getAllUnpersistedRecords();
        assertEquals(2, allRecords.size(), "Should always contain two items");
    }

    @Test
    void updateScannerRecords() {
        List<ScannerRecord> allRecords = cacheService2.getAllUnpersistedRecords();
        ScannerRecord recordToUpdate = allRecords.stream().filter(filter -> filter.getId().equalsIgnoreCase("yfdgjd")).findAny().get();
        if (recordToUpdate != null) {
            recordToUpdate.setWrittenToDb(true);

            HashMap<String, ScannerRecord> updatedRecords = new HashMap<>();
            updatedRecords.put(recordToUpdate.getId(), recordToUpdate);
            cacheService.cacheScannerRecords(updatedRecords);

            assertEquals(1, cacheService2.getAllUnpersistedRecords().size(), "Should now be less than 2 records because one should be updated");

        } else {
            fail("Failed to locate a required entry for the test");
        }
    }

    @BeforeEach
    public void createTestData() {
        ScannerRecord record = new ScannerRecord();
        record.setBarcode("fkdughdfi");
        record.setId("dfodifgj");
        record.setScanner("dskljfhdl");
        record.setMessageId("sdfusyhdf");
        record.setInsertTime(new Timestamp(System.currentTimeMillis()));
        record.setScanTime(new Timestamp(System.currentTimeMillis()));


        Map<String, ScannerRecord> items = new HashMap<>();
        items.put(record.getId(),record);

        assertNotNull(cacheService);

        record = new ScannerRecord();
        record.setBarcode("8576t847");
        record.setId("yfdgjd");
        record.setScanner("sdsdfjsj");
        record.setMessageId("sdmhfdsmfh");
        record.setInsertTime(new Timestamp(System.currentTimeMillis()));
        record.setScanTime(new Timestamp(System.currentTimeMillis()));

        items.put(record.getId(), record);
        cacheService.cacheScannerRecords(items);
    }
}