package com.example.s7datapump.batch;

import com.example.s7datapump.cache.CacheService;
import com.example.s7datapump.entity.ScannerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class InfinispanCacheCollectorTest extends InfinispanCacheCollector {

    @Autowired
    private CacheService cacheService;

    @Test
    void testDoRead() {
        this.doOpen();
        ScannerRecord scannerRecord = this.doRead();
        assertNotNull(scannerRecord, "Should not be null");
    }

    @Test
    void testDoOpen() {
    }

    @Test
    void testDoClose() {
        this.doOpen();
        this.doClose();
    }

//    @BeforeEach
    public void createTestData() {
        ScannerRecord record = new ScannerRecord();
        record.setBarcode("fkdughdfi");
        record.setId("dfodifgj");
        record.setScanner("dskljfhdl");
        record.setMessageId("sdfusyhdf");
        record.setInsertTime(new Timestamp(System.currentTimeMillis()));
        record.setScanTime(new Timestamp(System.currentTimeMillis()));


        Map<String, ScannerRecord> items = new HashMap<>();
        items.put(record.getId(),record);

        assertNotNull(cacheService);
//        cacheService.cacheScannerRecords(items);

        record = new ScannerRecord();
        record.setBarcode("8576t847");
        record.setId("yfdgjd");
        record.setScanner("sdsdfjsj");
        record.setMessageId("sdmhfdsmfh");
        record.setInsertTime(new Timestamp(System.currentTimeMillis()));
        record.setScanTime(new Timestamp(System.currentTimeMillis()));

        items.put(record.getId(), record);
        cacheService.cacheScannerRecords(items);
    }
}